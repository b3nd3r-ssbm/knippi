#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

mod cmd;
mod replay;
mod playback;
mod framedata;
pub mod dolphin;
pub mod config;
pub mod encoding;

use replay::*;
use framedata::*;
use playback::*;
use config::*;
use dolphin::*;
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use log::info;

fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::init();
    tauri::AppBuilder::new()
        .invoke_handler(|webview, arg| {
            use cmd::Cmd::*;
            match serde_json::from_str(arg) {
                Err(e) => Err(e.to_string()),
                Ok(command) => {
                    match command {
                        GetReplays { callback, error } => {
                            info!("Executing GetReplays");
                            tauri::execute_promise(webview, move || {
                                let config: Config = confy::load("knippi")?;
                                let reply = get_replays(&config.replay_folder_path.expect("Couldn't run get_replays()"));
                                Ok(serde_json::to_string(&reply)?)
                            },
                            callback,
                            error,
                            )
                        },
                        PlayReplays { replays } => {
                            info!("Executing PlayReplays");
                            let config: Config = confy::load("knippi").unwrap();
                            let stub = &config.replay_folder_path.clone();
                            let stub = stub.as_ref().unwrap().to_str();
                            let queue: Vec<_> = replays.iter().map(|item| {
                                let replay: &str = &item.file;
                                let path: PathBuf = [stub.unwrap(), replay].iter().collect();
                                QueueItem::new(path, item.start_frame, item.end_frame)
                            }).collect();
                            let playback = Playback::new(
                                "queue".to_string(),
                                queue
                            );
                            let json = serde_json::to_string(&playback).expect("Failed to parse as json");
                            let mut file = File::create("./playback.json").expect("Couldn't create file playback.json");
                            file.write_all(json.as_bytes()).expect("Unable to write to file");
                            play_from_json(PathBuf::from("playback.json"), config).unwrap();
                        },
                        GetConfig { callback, error } => {
                            info!("Executing GetConfig");
                            tauri::execute_promise(webview, move || {
                                let config: Config = confy::load("knippi").unwrap();
                                Ok(serde_json::to_string(&config)?)
                            },
                            callback,
                            error,
                            )
                        },
                        StoreConfig { config } => {
                            info!("Received StoreConfig");
                            confy::store("knippi", config).unwrap_or(());
                        },
                        GetFrameData { callback, error, path } => {
                            info!("Received GetFrameData");
                            tauri::execute_promise(webview, move || {
                                let config: Config = confy::load("knippi").unwrap();
                                let mut full_path = config.replay_folder_path.clone().unwrap().to_path_buf();
                                full_path.push(path);
                                let framedata = get_framedata(full_path);
                                Ok(serde_json::to_string(&framedata)?)
                            },
                            callback,
                            error,
                            )
                        },
                    }
                    Ok(())
                }
            }
        })
        .build()
        .run();
    Ok(())
}
