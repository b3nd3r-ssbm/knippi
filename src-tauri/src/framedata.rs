use peppi::character;
use peppi::frame;
use peppi::game::Start;
use peppi::parse::{FrameEvent, Handlers};
use peppi::stage;
use serde::Serialize;
use std::path::PathBuf;
use std::{fs, io};

#[derive(Debug, Serialize)]
pub struct GameData {
    pub stage: Option<stage::Stage>,
    pub players: Vec<PlayerData>,
    pub framedata: Vec<Vec<FrameData>>,
}

#[derive(Debug, Serialize)]
pub struct PlayerData {
    character: character::External,
}

#[derive(Debug, Serialize)]
pub struct FrameData {
    position: frame::Position,
    percent: f32,
}

impl From<frame::Post> for FrameData {
    fn from(frame: frame::Post) -> FrameData {
        FrameData {
            position: frame.position,
            percent: frame.damage,
        }
    }
}

impl Handlers for GameData {
    fn game_start(&mut self, start: Start) -> io::Result<()> {
        for player in start.players.iter() {
            if let Some(player) = player {
                let playerdata = PlayerData {
                    character: player.character,
                };
                self.players.push(playerdata);
            }
        }
        self.stage = Some(start.stage);
        Ok(())
    }
    fn frame_post(&mut self, post: FrameEvent<frame::Post>) -> io::Result<()> {
        let port = post.id.port;
        self.framedata[port as usize].push(post.event.into());
        Ok(())
    }
}

pub fn get_framedata(path: PathBuf) -> GameData {
    let file = fs::File::open(path).unwrap();
    let mut reader = io::BufReader::new(file);
    let mut handlers = GameData {
        stage: None,
        players: vec![],
        framedata: vec![vec![], vec![], vec![], vec![]],
    };
    peppi::parse(&mut reader, &mut handlers).unwrap();
    handlers
}
