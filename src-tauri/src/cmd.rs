use serde::Deserialize;
use std::path::PathBuf;
use super::replay::*;
use super::config::*;

#[derive(Deserialize)]
#[serde(tag = "cmd", rename_all = "camelCase")]
pub enum Cmd {
  GetReplays {
      callback: String,
      error: String,
  },
  PlayReplays {
      replays: Vec<Replay>,
  },
  GetConfig {
      callback: String,
      error: String,
  },
  StoreConfig {
      config: Config,
  },
  GetFrameData {
      callback: String,
      error: String,
      path: PathBuf,
  },
}
