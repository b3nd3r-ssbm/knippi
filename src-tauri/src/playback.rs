use serde::Serialize;
use std::path::PathBuf;
use rand::{thread_rng, Rng};

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Playback {
    mode: String,
    command_id: u16,
    output_overlay_files: bool,
    is_real_time_mode: bool,
    queue: Vec<QueueItem>,
}

impl Playback {
    pub fn new(mode: String, queue: Vec<QueueItem>) -> Playback {
        let mut rng = thread_rng();
        let command_id: u16 = rng.gen();
        Playback {
            mode,
            command_id,
            output_overlay_files: false,
            is_real_time_mode: false,
            queue,
        }
    }
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct QueueItem {
    path: PathBuf,
    start_frame: isize,
    end_frame: isize,
}

impl QueueItem {
    pub fn new(path: PathBuf, start_frame: isize, end_frame: isize) -> QueueItem {
        QueueItem {
            path,
            start_frame,
            end_frame,
        }
    }
}
