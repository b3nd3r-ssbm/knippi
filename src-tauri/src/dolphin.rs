use std::error::Error;
use std::path::PathBuf;
use std::process::Command;
use super::config::*;
use log::info;

pub fn play_from_json(path: PathBuf, config: Config) -> Result<(), Box<dyn Error>> {
    let command = Command::new(config.dolphin_path.expect("Invalid Dolphin path"))
        .arg("-i")
        .arg(path)
        .arg("-e")
        .arg(config.melee_iso_path.unwrap())
        .arg("-b")
        .output()
        .expect("Failed to run Dolphin");
    info!("{:?}", command);
    Ok(())
}
