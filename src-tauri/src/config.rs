use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub dolphin_path: Option<std::path::PathBuf>,
    pub replay_folder_path: Option<std::path::PathBuf>,
    pub melee_iso_path: Option<std::path::PathBuf>,
}

impl ::std::default::Default for Config {
    fn default() -> Self {
        Self {
            dolphin_path: None,
            replay_folder_path: None,
            melee_iso_path: None,
        }
    }
}
